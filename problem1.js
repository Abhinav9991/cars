const problem1=(arg,id) => {
    if (arg!== undefined && id!== undefined && arg.length !==0 && Array.isArray(arg)===true && Number.isInteger(id)){
    for(let i=0; i<arg.length;i++){
        if(arg[i].id===id){
            return arg[i];
        }
    }}
    else{
        return [];
    }
}

module.exports=problem1