const problem4=(arg)=>{
    if (arg!== undefined && arg.length !==0){
        let years=[];
        for (let i = 0; i < arg.length; i++) {
            years[i] = arg[i].car_year;
        }
        return years;
    }
    else{
        return [];
    }
}
module.exports=problem4